package com.clone.instagram.model;

import java.time.Instant;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
@Document(collection="users")
public class User {

	@Id
    private String id;
	
	private String name;
	
	private String username;
	
	private String password;
	
	private String email;
	
	@CreatedDate
	  private Instant createdAt;
	
	@LastModifiedDate
	private Instant updateAt;

	@JsonProperty("userProfile")
	private UserProfile userProfile;

	public User(String name,String username, String password, String email) {
		super();
		this.name=name;
		this.username = username;
		this.password = password;
		this.email = email;
	}

	
	public User(User user) {
		super();
		this.name = user.name;
		this.username = user.username;
		this.password = user.password;
		this.email = user.email;
		this.createdAt = user.getCreatedAt();
		this.updateAt = user.getUpdateAt();
		this.userProfile=user.userProfile;
	}


	public User() {
		super();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Instant getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Instant createdAt) {
		this.createdAt = createdAt;
	}

	public Instant getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Instant updateAt) {
		this.updateAt = updateAt;
	}


	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	

}
