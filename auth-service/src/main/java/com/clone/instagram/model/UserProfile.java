package com.clone.instagram.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
public class UserProfile {
	
	private String profilePictureUrl;
	private String birthDate;
	private String bio;
	public UserProfile() {
		super();
	}

}
