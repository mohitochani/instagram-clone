package com.clone.instagram.exception;

public class UsernameAlreadyExistsException extends RuntimeException{

	public UsernameAlreadyExistsException(String arg0) {
		super(arg0);
	}

}
