package com.clone.instagram.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.clone.instagram.exception.EmailAlreadyExistsException;
import com.clone.instagram.exception.UsernameAlreadyExistsException;
import com.clone.instagram.model.JwtRequest;
import com.clone.instagram.model.JwtResponse;
import com.clone.instagram.model.User;
import com.clone.instagram.repository.UserRepository;
import com.clone.instagram.service.UserService;
import com.clone.instagram.utility.JWTUtility;

@RestController
public class UserController {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private JWTUtility jwtUtility;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserService userService;
	
	private UserRepository repository;

	public UserController(UserRepository respository) {
		super();
		this.repository = respository;
	}
	
	@GetMapping("/users")
	public ResponseEntity<?> getAllUsers()
	{
		return ResponseEntity.ok(repository.findAll());
	}
	
	@GetMapping("/users/{username}")
	public List<User> getUserByUserName(@PathVariable("username") String username)
	{
		return repository.findByUsername(username);
	}
	
	@PostMapping("/users")
	public ResponseEntity<User> addUser(@RequestBody User user) throws Exception
	{
		if(repository.existsByUsername(user.getUsername())) {
			System.out.println("Username exists");
			throw new UsernameAlreadyExistsException("Username Exists");
		}
		if(repository.existsByEmail(user.getEmail())) {
			System.out.println("Email exists");
			throw new EmailAlreadyExistsException("Email Exists");
		}
		
		String password= user.getPassword();
		user.setPassword(passwordEncoder.encode(password));
		repository.save(user);
		
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}
	
	@GetMapping("/")
	public String home() throws Exception
	{
		return "Welcome to Daily Code Buffer";
	}
				
	@PostMapping("/authenticate")
	public JwtResponse authenticate(@RequestBody JwtRequest jwtRequest) throws Exception {
		
		try {
		authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						jwtRequest.getUsername(),jwtRequest.getPassword())
				);
		}catch(BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS",e);
		}
		System.out.println("Valid Creds");
		final UserDetails userDetails= userService.loadUserByUsername(jwtRequest.getUsername());
		
		final String token = jwtUtility.generateToken(userDetails);
		System.out.println("Token:" + token);
		return new JwtResponse(token);
	}
	
	

}
