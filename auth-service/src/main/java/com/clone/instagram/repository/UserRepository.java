package com.clone.instagram.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.clone.instagram.model.User;

@Repository
public interface UserRepository extends MongoRepository<User,String> {

	List<User> findByUsername(String username);

	boolean existsByUsername(String username);

	boolean existsByEmail(String email);

}
